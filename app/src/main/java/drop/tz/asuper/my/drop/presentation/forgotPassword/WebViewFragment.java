package drop.tz.asuper.my.drop.presentation.forgotPassword;


import android.annotation.SuppressLint;
import android.databinding.ObservableBoolean;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import drop.tz.asuper.my.drop.R;
import drop.tz.asuper.my.drop.databinding.FragmentWebViewBinding;
import drop.tz.asuper.my.drop.presentation.core.fragments.BaseMainFragment;
import drop.tz.asuper.my.drop.presentation.dialogs.LoadingDialog;

public class WebViewFragment extends BaseMainFragment<FragmentWebViewBinding> {

    private ObservableBoolean mShowWebView = new ObservableBoolean();

    public ObservableBoolean getShowWebView() {
        return mShowWebView;
    }

    public static WebViewFragment newInstance() {
        WebViewFragment fragment = new WebViewFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_web_view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final LoadingDialog loadingDialog = new LoadingDialog(getContext());
        loadingDialog.show();

        //см функцию addLoadingDialog в логин фрагменте. если вынести то что там в диалог, можно и повторно тут использовать

        getBinding().wvWebPage.setWebViewClient(new WebViewClient() {
            //за чем єто все?
            //onPageStarted - показал прогрес
            //onPageFinished - спрятал и хватит.
            // если обработал shouldOverrideUrlLoading то обработай и действие по нажатию кнопки бек
            // (хотя задача реализовать навигацию по страницам не была поставлена в тз и на shouldOverrideUrlLoading можно было забить).

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                showAnim();
            }

            public void onPageFinished(WebView view, String url) {
                removeAnim();
            }

            private void showAnim() {
                if (mShowWebView.get()) {
                    mShowWebView.set(false);
                    loadingDialog.show();
                }
            }

            private void removeAnim() {
                mShowWebView.set(true);
                loadingDialog.dismiss();
            }
        });

        getBinding().wvWebPage.getSettings().setJavaScriptEnabled(true);
        getBinding().wvWebPage.loadUrl(getString(R.string.FORGOT_PASSWORD_URL));
    }


    @Override
    public void onResume() {
        super.onResume();
        getBinding().setWebViewFragment(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().setWebViewFragment(null);
    }
}
