package drop.tz.asuper.my.drop.presentation.login;


public interface LoginNavigator {
    void showSplashFragment();

    void showLoginFragment();

    void showWebViewFragment();

    void showMainFragment(String name, String password);
}
