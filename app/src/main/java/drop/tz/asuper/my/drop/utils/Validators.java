package drop.tz.asuper.my.drop.utils;


import android.content.Context;
import android.widget.EditText;

import java.util.zip.CheckedOutputStream;

import drop.tz.asuper.my.drop.R;

public class Validators {
    // то же что и с функцией ниже
    public static boolean validateName(EditText editText, Context context) {
        String name = editText.getText().toString();
        if (name.isEmpty()) {
            editText.setError(context.getString(R.string.validate_name_is_empty));
            return false;
        } else if (name.length() < 4) {
            editText.setError(context.getString(R.string.validate_name_min_len));
            return false;
        } else if (!name.matches("^[^\\s]+$")) {
            editText.setError(context.getString(R.string.validate_name_contain_spaces));
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    //если уже передаешь EditText то запихни в отдельный хелпер класс
    //или юзай getBinding().etPassword
    public static boolean validatePassword(EditText editText, Context context) {
        String pass = editText.getText().toString();
        if (pass.isEmpty()) {
            editText.setError(context.getString(R.string.validate_pass_is_empty));
            return false;
        } else if (pass.length() < 8) {
            editText.setError(context.getString(R.string.validate_pass_min_len));
            return false;
        } else if (!pass.matches("^[A-Za-z_]+$")) {
            editText.setError(context.getString(R.string.validate_pass_latin_symbols));
            return false;
        } else if (!pass.matches("^(?:(.)((?=_)|(?!\\1)))*$")) {    //   ([A-Za-z])\1+
            editText.setError(context.getString(R.string.validate_pass_near_letters));
            return false;
        } else {

            int counter = 0;
            for (char element : pass.toCharArray()) {
                if (element == '_') counter++;
            }
            if (counter < 2) {
                editText.setError(context.getString(R.string.validate_pass_underscore));
                return false;
            } else {
                editText.setError(null);
                return true;
            }
        }
    }
}
