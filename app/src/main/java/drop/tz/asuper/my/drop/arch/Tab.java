package drop.tz.asuper.my.drop.arch;


public enum Tab {
    SPALSH(0), LOGIN(1);

    private final int mId;

    Tab() {
        mId = -1;
    }

    Tab(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public static Tab fromName(String name) {
        for (Tab type : Tab.values()) {
            if (type.toString().equals(name)) {
                return type;
            }
        }
        return null;
    }
}
