package drop.tz.asuper.my.drop.arch;


public interface RequestCallback {
    void onDataReady(int isValid);
}
