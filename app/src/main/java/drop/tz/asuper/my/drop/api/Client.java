package drop.tz.asuper.my.drop.api;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;

import drop.tz.asuper.my.drop.arch.RequestCallback;
import drop.tz.asuper.my.drop.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Client {

    //работа с апи запросы - ответы в отдельный пакет Апи

    private static Client instance;
    private String TAG = getClass().getSimpleName();

    private String mName;
    private String mUrl;
    private RequestCallback mCallback;

    private Thread mThread;

    public static Client getInstance() {
        if (instance == null) {
            instance = new Client();
        }
        return instance;
    }

    public void onPause() {
        mCallback = null;
        try {
            if (mThread != null) {
                mThread.interrupt();
                mThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //в отдельный класс типа NetworkUtils или туда где делаются запросы
    // проверяет только подключение, еще бы проверять на реальное наличие интернета
    public boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager manager = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager != null) {
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            boolean isAvailable = false;
            if (networkInfo != null && networkInfo.isConnected()) {
                isAvailable = true;
            }
            return isAvailable & isOnline();
        } else {
            return false;
        }
    }

    private boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void sendRequest(String name, String url, RequestCallback requestCallback) {
        mName = name;
        mUrl = url;
        mCallback = requestCallback;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                String result = "";
                String address;
                address = mUrl.concat(mName);
                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url(address)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    result = response.body().string();
                    if (!response.isSuccessful()) {
                        throw new Exception("Unexpected code " + response);
                    }
                    result = response.code() == 200 ? result : "";
                } catch (Exception e) {
                }

                int res;

                if (TextUtils.isEmpty(result)) {
                    res = Constants.SERVER_ERROR;
                } else {
                    if (result.contains("No  results.")) {
                        res = Constants.USER_NOT_VALID;
                    } else {
                        res = Constants.USER_VALID;
                    }
                }
                if (mCallback != null) {
                    mCallback.onDataReady(res);
                }
            }
        });

        mThread.start();
    }
}