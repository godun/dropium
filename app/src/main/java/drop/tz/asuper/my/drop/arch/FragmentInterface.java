package drop.tz.asuper.my.drop.arch;


public interface FragmentInterface {
    boolean onBack();

    FragmentInterface getActivityContext();

    void doFragment(FragmentOperation operation);

    TabManager getTabManager();
}
