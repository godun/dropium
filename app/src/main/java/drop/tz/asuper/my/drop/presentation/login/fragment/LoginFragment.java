package drop.tz.asuper.my.drop.presentation.login.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import drop.tz.asuper.my.drop.R;
import drop.tz.asuper.my.drop.api.Client;
import drop.tz.asuper.my.drop.arch.RequestCallback;
import drop.tz.asuper.my.drop.databinding.FragmentLoginBinding;
import drop.tz.asuper.my.drop.presentation.core.fragments.BaseMainFragment;
import drop.tz.asuper.my.drop.Constants;
import drop.tz.asuper.my.drop.presentation.dialogs.LoadingDialog;
import drop.tz.asuper.my.drop.utils.UIUtils;
import drop.tz.asuper.my.drop.utils.Validators;

public class LoginFragment extends BaseMainFragment<FragmentLoginBinding> {
    private Handler mUiThread;
    private LoadingDialog mLoadingDialog;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDoLogin:
                userLogin();
                break;
            case R.id.tvForgotPass:
                if (!Client.getInstance().isNetworkAvailable(getActivity())) {
                    Toast.makeText(getContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else if (getLoginNavigator() != null) {
                    getLoginNavigator().showWebViewFragment();
                }
                break;
        }
    }

    private void userLogin() {
        getBinding().btnDoLogin.setEnabled(false);
        if (!Client.getInstance().isNetworkAvailable(getActivity())) {
            Toast.makeText(getContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            UIUtils.hideKeyboard(getContext(), getView());
            getBinding().btnDoLogin.setEnabled(true);
        } else if (Validators.validateName(getBinding().etUserName, getContext()) &
                Validators.validatePassword(getBinding().etLoginPassword, getContext())) {
            UIUtils.hideKeyboard(getContext(), getView());
            mLoadingDialog = new LoadingDialog(getContext());
            mLoadingDialog.show();
            Client.getInstance().sendRequest(getBinding().etUserName.getText().toString(), getString(R.string.base_url), new RequestCallback() {
                @Override
                public void onDataReady(final int isValid) {
                    mUiThread.post(new Runnable() {
                        @Override
                        public void run() {
                            mLoadingDialog.dismiss();
                            switch (isValid) {
                                case Constants.SERVER_ERROR:
                                    UIUtils.addInfoDialog(getString(R.string.dialog_error_title), getString(R.string.dialog_error_message), getContext());
                                    break;
                                case Constants.USER_NOT_VALID:
                                    UIUtils.addInfoDialog(getString(R.string.dialog_user_not_found), getString(R.string.dialog_user_not_found_message), getContext());
                                    break;
                                case Constants.USER_VALID:
                                    if (getLoginNavigator() != null) {
                                        getLoginNavigator().showMainFragment(getBinding().etUserName.getText().toString(), getBinding().etLoginPassword.getText().toString());
                                    }
                                    break;
                            }
                            getBinding().btnDoLogin.setEnabled(true);
                        }
                    });

                }
            });
        } else {
            getBinding().btnDoLogin.setEnabled(true);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                switch (i) {
                    case EditorInfo.IME_ACTION_NEXT:
                        Validators.validateName(getBinding().etUserName, getContext());
                        break;
                    case EditorInfo.IME_ACTION_DONE:
                        userLogin();
                        return true;
                }
                return false;
            }
        };

        getBinding().etUserName.setOnEditorActionListener(onEditorActionListener);
        getBinding().etLoginPassword.setOnEditorActionListener(onEditorActionListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        mUiThread = new Handler(Looper.getMainLooper());
        UIUtils.hideKeyboard(getContext(), getView());
        getBinding().setLoginFragment(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mUiThread != null) {
            mUiThread.removeCallbacksAndMessages(this);
        }
        getBinding().setLoginFragment(null);
        if (mLoadingDialog != null)
            mLoadingDialog.dismiss();
        Client.getInstance().onPause();
        getBinding().btnDoLogin.setEnabled(true);
    }
}