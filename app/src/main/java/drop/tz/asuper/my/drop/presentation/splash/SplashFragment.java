package drop.tz.asuper.my.drop.presentation.splash;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.LinearInterpolator;

import drop.tz.asuper.my.drop.R;
import drop.tz.asuper.my.drop.databinding.FragmentSplashBinding;
import drop.tz.asuper.my.drop.presentation.core.fragments.BaseMainFragment;
import drop.tz.asuper.my.drop.Constants;

public class SplashFragment extends BaseMainFragment<FragmentSplashBinding> {
    private Handler mUiHandler;

    //хендлер не нужен
    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUiHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mUiHandler != null) {
            mUiHandler.removeCallbacksAndMessages(this);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
        * у тебя тут были мутки с хендлером. он не нужен. можно обойтись только анимацией. сравни с своим.
        *
        * */

        final ObjectAnimator splashAnimation = ObjectAnimator.ofFloat(getBinding().ivLoadingLogo, View.ROTATION_Y, 0.0f, 180f);
        splashAnimation.setDuration(Constants.SPLASH_ANIM_DURATION);
        splashAnimation.setRepeatCount(2);
        splashAnimation.setInterpolator(new LinearInterpolator());
        splashAnimation.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                splashAnimation.removeAllListeners();
                if (getLoginNavigator() != null) {
                    getLoginNavigator().showLoginFragment();
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }


        });
        splashAnimation.start();
    }
}
