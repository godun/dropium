package drop.tz.asuper.my.drop.presentation.core.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import drop.tz.asuper.my.drop.arch.AppBaseFragment;
import drop.tz.asuper.my.drop.presentation.login.LoginNavigator;

public abstract class BaseMainFragment<ViewBinding extends ViewDataBinding> extends AppBaseFragment {
    private ViewBinding mBinding;
    private LoginNavigator mLoginNavigator;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mLoginNavigator = ((LoginNavigator) context);
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement MainNavigator");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return DataBindingUtil.inflate(inflater, getLayoutId(), container, false).getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding = DataBindingUtil.bind(view);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLoginNavigator = null;
    }

    protected void onClick(View view) {
    }

    protected abstract int getLayoutId();

    public ViewBinding getBinding() {
        return mBinding;
    }

    public LoginNavigator getLoginNavigator() {
        return mLoginNavigator;
    }
}
