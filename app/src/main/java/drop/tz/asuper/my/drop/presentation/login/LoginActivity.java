package drop.tz.asuper.my.drop.presentation.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.WindowManager;


import drop.tz.asuper.my.drop.R;
import drop.tz.asuper.my.drop.arch.BaseActivity;
import drop.tz.asuper.my.drop.arch.FragmentAnimation;
import drop.tz.asuper.my.drop.arch.FragmentOperation;
import drop.tz.asuper.my.drop.arch.Tab;
import drop.tz.asuper.my.drop.arch.TabGroup;
import drop.tz.asuper.my.drop.presentation.forgotPassword.WebViewFragment;
import drop.tz.asuper.my.drop.presentation.login.fragment.LoginFragment;
import drop.tz.asuper.my.drop.presentation.main.MainFragment;
import drop.tz.asuper.my.drop.presentation.splash.SplashFragment;

public class LoginActivity extends BaseActivity implements LoginNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataBindingUtil.setContentView(this, R.layout.activity_login);

        /*
        * норм, но лучше бы с сырым fragment manager поработал :)
        * */

        if (getTabManager().isNewInstance()) {
            getTabManager().init(new TabGroup()
                    .container(R.id.frameFragmentContainer)
                    .init(Tab.SPALSH)
                    .init(Tab.LOGIN));
        }

        showSplashFragment();
    }

    @Override
    public void showSplashFragment() {
        doFragment(FragmentOperation.instance().tab(Tab.SPALSH).fragment(SplashFragment.newInstance()).setAndShow());
    }

    @Override
    public void showLoginFragment() {
        doFragment(FragmentOperation.instance().tab(Tab.LOGIN).fragment(LoginFragment.newInstance()).setAndShow().animation(FragmentAnimation.instance(R.anim.in_anim, R.anim.out_anim)));
    }

    @Override
    public void showWebViewFragment() {
        doFragment(FragmentOperation.instance().tab(Tab.LOGIN).fragment(WebViewFragment.newInstance()).push());
    }

    @Override
    public void showMainFragment(String name, String password) {
        doFragment(FragmentOperation.instance().tab(Tab.LOGIN).fragment(MainFragment.newInstance(name, password)).push());
    }
}
