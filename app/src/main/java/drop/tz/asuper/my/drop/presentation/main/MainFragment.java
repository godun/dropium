package drop.tz.asuper.my.drop.presentation.main;


import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.Nullable;

import drop.tz.asuper.my.drop.R;
import drop.tz.asuper.my.drop.databinding.FragmentMainBinding;
import drop.tz.asuper.my.drop.presentation.core.fragments.BaseMainFragment;
import drop.tz.asuper.my.drop.Constants;

public class MainFragment extends BaseMainFragment<FragmentMainBinding> {


    private ObservableField<String> mName = new ObservableField<>();
    private ObservableField<String> mPassword = new ObservableField<>();

    public ObservableField<String> getName() {
        return mName;
    }

    public ObservableField<String> getPassword() {
        return mPassword;
    }

    public static MainFragment newInstance(String name, String password) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        /*
        * это перенес в константы
        * */
        args.putString(Constants.NAME_KEY, name);
        args.putString(Constants.PASS_KEY, password);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mName.set(getArguments().getString(Constants.NAME_KEY));
        mPassword.set(getArguments().getString(Constants.PASS_KEY));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        * имя нормальное variable. не handler
        * */
        getBinding().setMainFragment(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBinding().setMainFragment(null);
    }
}
