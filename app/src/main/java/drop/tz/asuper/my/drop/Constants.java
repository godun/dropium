package drop.tz.asuper.my.drop;

/// консатнты в кореневой пакет
/*
* я сюда перенес SPLASH_ANIM_DURATION и ключи. им там не нместо где они были
* */
public class Constants {



    public static final int USER_NOT_VALID = 0;
    public static final int USER_VALID = 1;
    public static final int SERVER_ERROR = 2;
    public static final int SPLASH_ANIM_DURATION = 1000;
    public static final String NAME_KEY = "name";
    public static final String PASS_KEY = "pass";
}
